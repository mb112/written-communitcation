# Open Systems Interconnection Model

### Definition

The Open Systems Interconnection model (OSI model) is a conceptual model that describes the universal standard of
communication functions of a telecommunication system or computing system, without any regard to the system's underlying
internal technology and specific protocol suites.
It was developed by the International Organization for Standardization (ISO) in 1984.
The goal of the OSI model is that all diverse communication systems have standard protocol for all networked
communication.

### Layers of OSI Model

There are the seven OSI layers. Each layer is package of protocols, with different functions.
Following are the 7 layers:

1. Application Layer
2. Presentation Layer
3. Session Layer
4. Transport Layer
5. Network Layer
6. Data-Link Layer
7. Physical Layer

![7 layers](https://static.javatpoint.com/tutorial/computer-network/images/osi-model2.png)

### Physical Layer

It is the lowest layer of the model.
It is responsible for actual physical connection between devices.
It contains information in form of bits.
While sending data( received from Data-Link layer), it converts the data packets(digital bits) into electrical in case
of cooper wire,
radio in case of air, or light in case optical wire signals.
At the receiver end it converts signal into bits and pass it Data-link layer as a frame.

**Functions of Physical layer:**

1. Physical topologies: It specifies the way in which the different, devices/nodes are arranged in a network.
2. Bit synchronization: It provides the synchronization of the bits by providing a clock. This clock controls both
   sender and receiver thus providing synchronization at bit level.
3. Transmission mode: It also defines the way in which the data flows between the two connected devices. There are three
   ways i.e. Simplex, half-duplex and full-duplex.

### Data-Link Layer

It is second last layer of the model.The main function of this layer is to make sure data transfers without any error
from one node to another, over the physical layer. It is embedded in the Network interface card(NIC) of the computer.
Data Link Layer is divided into two sublayers which are Logical Link Control (LLC) and Media Access Control (MAC).

**Function of Data-Link Layer:**

1. Framing : It encapsulates the data into a frame with a head and a tail, so that the transmitted data packet is
   meaningful to the receiver.
2. Physical addressing : Head of the frame consist of physical address of sender or receiver.
3. Error control : It provides the mechanism of error control in which it detects and retransmits damaged or lost
   frames.
4. Flow Control : The data rate must be constant on both sides else the data may get corrupted thus, flow control
   coordinates the amount of data that can be sent before receiving acknowledgement.
5. Access control : When a single communication channel is shared by multiple devices, the data
   link layer uses protocols to determine which device has control over the channel at a given time.

### Network Layer

The network layer works for the transmission of data from one host to the other located in different networks. It uses
protocols such as OSPF,BGP,IS-IS to determine the best possible way for data delivery

**Functions of Network Layer:**

1. Routing: The network layer protocols determine which route is suitable from source to destination.
2. Logical Addressing: The sender & receiver’s IP addresses are placed in the header by the network layer. Such an
   address distinguishes each device uniquely and universally.

### Transport Layer

It is the middle layer of the OSI model. It ensures that messages are transmitted in the order in which they are sent
and there is no duplication of data.
Also provides the acknowledgement of the successful data transmission and re-transmits the data if an error is found.
There are two Protocols of this layer:

- Transmission Control Protocol (TCP) : Connection-oriented Transmission is done by TCP.It provides feedback so that, any
  data loss can be retransmitted. It is used in places where entire data transmission is must.Example - World Wide Web(
  www), FTP, E-Mails
- User Datagram Protocol (UDP) : Connectionless Transmission is done by UDP.It is faster than TCP because it does not
  provide any feedback. It is used where it does not matter if whole data is delivered or not .Example: Online Movie
  Streaming, Video gaming, DNS request

![Transfer Layer](https://static.javatpoint.com/tutorial/computer-network/images/osi-model7.png)

**Function of Transport Layer:**

1. Segmentation and Reassembly : It receives the data from session layer and divide it into small data units known as,
   segments. Each segment contains a port no. and sequence no. . The port number helps to deliver the correct data to
   correct application and sequence no. helps to deliver it in correct order.
2. Flow Control : It controls the amount of data being transmitted.
3. Error control : It makes sure that the received is not corrupted or there was no data loss. In either of the case it
   uses Automatic Repeat Request to retransmit the data.

### Session Layer
This layer is responsible for the establishment of connection, maintenance of sessions, authentication and authorise.

**Functions of Session Layer:**
1. Session establishment, maintenance, and termination: The layer allows the two processes to establish, use and terminate a connection.
2. Synchronization: This layer allows a process to add checkpoints. These points help to identify the error so that the data is re-synchronized properly, and ends of the messages are not cut prematurely and data loss is avoided.
3. Dialog Controller: The session layer allows two systems to start communication with each other in half-duplex or full-duplex.

### Presentation Layer
It receives data from Application layer. It then converts the data received into required format.

**Function of Presentation Layer:**
1. Translation: The data received is alphanumeric format, then it is converted into binary format. For example, ASCII to EBCDIC.
2. Data Compression : Before data is transmitted , this layer reduces the bit that are required to represent the data.
3. Encryption/Decryption : To maintain the integrity of the data transmitted, the data is encrypted. Protocol such as SSL(Secure Socket Layer) is used.

### Application Layer
It is the top most layer of the OSI model, and it used by network applications. The protocols of this layer forms the basis for services like File transfer, Web Surfing, Email.

### Refrences

- [OSI Model Explained By TechTerms](https://youtu.be/vv4y_uOneC0)
- [OSI Model - Wikipedia](https://en.wikipedia.org/wiki/OSI_model#Layer_1:_Physical_layer)
- [OSI Model - javaTpoint](https://www.javatpoint.com/osi-model)
- [OSI Model - GeeksForGeeks](https://www.geeksforgeeks.org/layers-of-osi-model/)
- [OSI Model - Forcepoint](https://www.forcepoint.com/)


